# National Parks Lookup

This application allows it's user to search by state and received a returned list of national parks in that state. The application also allows the user to view information about that specific state. This application will make use of the National Parks Service API. 

## Tech Stack

- Node.js v14.15.0 (backend api)
- Create React App 3.4.1 (frontend client)
- NPM 6.14.8
- Graphql


## Application Architecture Design 

Originally, the plan was to create a Node.js api backend that would fetch data from the National Parks Service API and the National Weather Service API to create a mock database. Then the React front end would make requests to the Graphql api layer to the backend to fetch that data for the user. I would use Redux Store for managing the state of the application on the client-side as well as Jest for all testing. Docker-Compose will be in charge of creating a simple build of the client and server side on the same network and run the application easily. 

## Run the Application

- docker-compose up