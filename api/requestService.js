const fetch = require('node-fetch');
require('dotenv').config();

const basicHeaders = { Accept: "*/*" };

const corsHackUrl = "https://cors-anywhere.herokuapp.com/";
const nationalParksServiceEndpoint = process.env.NATIONAL_PARKS_SERVICE_API_BASE_ENDPOINT;
const nationalParkServiceApiKey = process.env.NATIONAL_PARKS_SERVICE_API_KEY;

async function npsAPISearchByParkCode(parkCode) {
    // const requestUrl = `${nationalParksServiceEndpoint}parks?parkCode=${parkCode}&api_key=${nationalParkServiceApiKey}`;
    const requestUrl = `${nationalParksServiceEndpoint}parks?api_key=${nationalParkServiceApiKey}`;
    const requestOptions = {
      headers: basicHeaders,
      method: "POST",
      credentials: "same-origin"
    };
  
    return await fetch(corsHackUrl + requestUrl, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log(requestUrl);
        console.log(json);
      })
      .catch(error => error);
};

async function fetchAllParks() {
    console.log('#### called fetch function ####')
    const requestUrl = `${nationalParksServiceEndpoint}parks?api_key=${nationalParkServiceApiKey}`;
    const requestOptions = {
      headers: basicHeaders,
      method: "POST",
      credentials: "same-origin"
    };
console.log(corsHackUrl + requestUrl);
    return await fetch(requestUrl)
      .then(res => res.json())
      .then(json => {
        
        console.log(json);
      })
      .catch(error => error);
};

  module.exports.npsAPISearchByParkCode = npsAPISearchByParkCode;
  module.exports.fetchAllParks = fetchAllParks;