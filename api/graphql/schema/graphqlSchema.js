const { buildSchema } = require('graphql');

module.exports = buildSchema(`
type Park {
    _id: ID!
    name: String!
    state: String!
    parkCode: String!
}
            
input ParkInput {
    name: String!
    state: String!
    parkCode: String!
}
            
type RootQuery {
    parks: [Park!]!
}

schema {
    query: RootQuery
}
`);