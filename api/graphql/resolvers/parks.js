const parks = [
    {
        id: 1,
        name: "Acadia National Park",
        state: "Maine",
        parkCode: "acad"
        // data: {

        // } 
    },
    {
        id: 2,
        name: "Arches National Park",
        state: "Utah",
        parkCode: "arch" //check
    },
    {
        id: 3,
        name: "Bandelier National Monument",
        state: "New Mexico",
        parkCode: "band"
    },
    {
        id: 4,
        name: "Crater Lake",
        state: "Oregon",
        parkCode: "crat"
    },
    {
        id: 5,
        name: "Denali National Park",
        state: "Alaska",
        parkCode: "denali"
    },
    {
        id: 6,
        name: "Grand Canyon",
        state: "Arizona",
        parkCode: "grand"
    },
    {
        id: 7,
        name: "Death Valley",
        state: "California",
        parkCode: "death"
    },
    {
        id: 8,
        name: "Mesa Verde",
        state: "Colorado",
        parkCode: "mesa"
    },
    {
        id: 9,
        name: "Everglades",
        state: "Florida",
        parkCode: "ever"
    },
    {
        id: 10,
        name: "Indiana Dunes",
        sate: "Indiana",
        parkCode: "dunes"
    },
    {
        id: 11,
        name: "Mammoth Cave",
        state: "Kentucky",
        parkCode: "cave"
    },
    {
        id: 12,
        name: "Grand Teton National Park",
        state: "Wyoming",
        parkCode: "teton"
    },
    {
        id: 13,
        name: "Olympic National Park",
        state: "Washington",
        parkCode: "olympic"
    },
    {
        id: 14,
        name: "Shenandoah",
        state: "Virginia",
        parkCode: "shen"
    },
    {
        id: 15,
        name: "Big Bend National Park",
        state: "Texas",
        parkCode: "bend"
    }
];

module.exports = {
    parks: () => {
        return parks;
    }
};