const parksResolver = require('./parks');

const rootResolver = {
    ...parksResolver
};

module.exports = rootResolver;