const requestService = require('./requestService');

function listAllParks() {
    // search through the json and format it into nice objects for the graphql to read
    console.log(requestService.fetchAllParks());
};

module.exports.listAllParks = listAllParks;