import React, { Component } from 'react';

class Park extends Component {
  state = {
    creating: false,
    parks: []
  };

  componentDidMount() {
    this.fetchPark();
  }

    fetchParks() {
      console.log('fetching all single....')
      const requestBody = {
        // add filter with parkCode to the grpahql
        query: `
            query { 
              parks {
                name
                parkCode
              }
            }
          `
      };

    const targetUrl = 'http://localhost:8000/graphql';

    fetch(targetUrl, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => {
      if (res.status !== 200 && res.status !== 201) {
        throw new Error(res.status + ': failed');
      }
      return res.json();
    })
    .then(resData => {
      const  parks = resData.data.parks;
      this.setState({ parks: parks });
    })
    .catch(err => {
      console.log(err);
    });
  };

  render() {
    const park = this.state.parks.map(park => {
      return (
      <li className="parkItem">
        {park}
      </li>
    );
  });
    return (
      <section>
        <h2>Park View</h2>
        <ul className="park">{park}</ul>
      </section>
    )};
};

export default Park;