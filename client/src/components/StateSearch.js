import React from "react";
import { Button, Form } from "semantic-ui-react";

const StateSearch = () => {

  return (
    <Form>
      <Form.Field>
        <label>Enter State: </label>
        <input
          name="state"
          placeholder="State"
          autoComplete="off"
        />
      </Form.Field>
      <Button type="submit">
        Search
      </Button>
    </Form>
  );
};

export default StateSearch;