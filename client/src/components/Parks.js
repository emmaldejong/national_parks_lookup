import React, { Component } from 'react';

class Parks extends Component {
  state = {
    creating: false,
    parks: []
  };

  componentDidMount() {
    this.fetchParks();
  }

    fetchParks() {
      console.log('fetching all parks....')
      const requestBody = {
        // add filter with parkCode to the grpahql???
        query: `
            query { 
              parks {
                name
                parkCode
              }
            }
          `
      };

    const targetUrl = 'http://localhost:8000/graphql';

    fetch(targetUrl, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => {
      if (res.status !== 200 && res.status !== 201) {
        throw new Error(res.status + ': failed');
      }
      return res.json();
    })
    .then(resData => {
      const  parks = resData.data.parks;
      this.setState({ parks: parks });
    })
    .catch(err => {
      console.log(err);
    });
  };

  render() {
    const parksList = this.state.parks.map(park => {
      return (
      <li className="parksListItem">
        {park.name}
      </li>
    );
  });
    return (
      <section>
        <h2>Parks List</h2>
        <ul className="parksList">{parksList}</ul>
      </section>
    )};
};

export default Parks;