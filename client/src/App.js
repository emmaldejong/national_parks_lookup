// import logo from './national_park_photo.jpg';
import { Container, Header, Grid } from 'semantic-ui-react';
import Parks from './components/Parks';
import StateSearch from './components/StateSearch';
// import Park from './components/Park';

import './App.css';

function App() {
  return (
      <Container className="App" style={{ marginTop: "3rem" }}>
        <Header as="h1">National Parks Lookup App</Header>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          columns={2}>
            <Grid.Column>
              <Parks />
            </Grid.Column>
            <Grid.Column>
              <StateSearch />
            </Grid.Column>
            {/* <Grid.Column>
              <Park />
            </Grid.Column> */}
          </Grid>
      </Container>
  );
}

export default App;